<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class todo extends Model
{
    //accessor function
//     public function getbodyAttribute($value)
//     {
//         return ucfirst($value);
//     }

    //mutator function
    public function settitleAttribute($value)
    {
        $this->attributes['title'] = ucfirst($value);
    }
}
