@extends('layouts.app') @section('title', 'create page') @section('body')
<br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center">Create Todo List</h5>
            <hr class="col-md-4">
            <div class="back text-right">
                <a href="/todo" class="btn btn-success">Back</a>
            </div>
            <form action="/todo" method="post">
                {{csrf_field()}}

                <fieldset>
                    <div class="form-group">
                        <input type="text" name="title" id="title" class="form-control" value="@yield('editTitle')" placeholder="Title" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="body" name="body"  placeholder="body" rows="3">@yield('editBody')</textarea>
                    </div>
                </fieldset>
                <button type="submit" class="btn btn-primary">Submit</button>
                </fieldset>
            </form>
            @section('editMethod')
            @show
           <div>
                <br>
@include('todo.partials.error')
           </div>
        </div>
    </div>
</div>
@endsection