@extends('layouts.app')
@section('title', 'Inside todo')
@section('body')
    <br>
    <h5 class="text-center text-info">Inside todo table through id</h5>
    <hr>
    <div class="container">

        <div class="row">

            <div class="col-md-4 offset-md-4">
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">{{$item->title}}</h5>
                            {{--<small></small>--}}
                        </div>
                        <p class="mb-1">{{$item->body}}</p>
                        <small>{{$item->created_at->diffforHumans()}}</small>
                    </a>

                </div>
            </div>

        </div>


    </div>
@endsection