@extends('layouts.app')
@section('title', 'welcome')
@section('body')
    <br>
    @if(session()->has('messege'))
        <div class="alert alert-success" role="alert">
            <strong>{{session()->get('messege')}}</strong>
        </div>
    @endif


    <div class="addItem text-right">
        <a href="todo/create" class="btn btn-primary"><i style="font-size: xx-large;" class="fas fa-list-ul te"></i></a>
    </div>
    <br>
    <h1 class="text-center bg-secondary text-white">Home Todo List</h1>
    <div class="container">

        <div class="col-md-6 offset-md-3">
            <div class="row">
                <ul class="list-group col-md-8">

                    {{--<li class="bg-{{$bgcol}}">hi</li>--}}
                    @foreach($todos as $i=>$todo)
                        @php
                            $colors = ['danger','warning','success','info','secondary','dark'];
                            //$randomize = $colors[mt_rand(0, count($colors)-1)];
                            $index = array_rand($colors, 1);
                            $selected = $colors[$index];
                            unset($colors[$index]);
                        @endphp
                        <li class="list-group-item bg-{{$selected}}">
                            <a class="text-white" href="{{'/todo/'.$todo->id}}">{{$todo->title}}</a>
                            <span class="float-right">{{$todo->created_at->diffforHumans()}}</span>

                        </li>

                    @endforeach
                </ul>
                <ul class="list-group col-md-4">
                    @foreach($todos as $todo)

                        <li class="list-group-item">
                            <a href="{{'/todo/'.$todo->id.'/edit'}}"><i class="far fa-edit"></i></a>
                            <form class="float-right" action="{{'/todo/'.$todo->id}}" method="post">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button type="submit" style="border: none;padding: 0;background-color: snow;"><i
                                            class="fas fa-trash-alt text-success"></i></button>
                            </form>
                        </li>

                    @endforeach
                </ul>
            </div>

        </div>


    </div>
@endsection